using Godot;
using System;

namespace MyKindom.UnitSystem
{
	public partial class UnitSpawner : Node2D
	{
		[Export]
		public PackedScene UnitScene { get; set; }

		private UnitBase m_currentUnit { get; set; }

		public override void _Ready()
		{
        }

		public void SpawnUnit()
		{
			string unitId = "abcd";
			bool ret = InstantiateUnitById(ref unitId);
			if (ret)
			{
				m_currentUnit.Position = this.Position;
				UnitManager.Instance.AddTargetUnitToManager(m_currentUnit);
			}
		}

		public bool InstantiateUnitById(ref string unitId)
		{
			UnitBase unit = UnitScene.Instantiate<UnitBase>();
			unit.m_unitId = unitId;
			m_currentUnit = unit;

			return true;
		}
	}
}