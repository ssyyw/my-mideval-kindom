using Godot;
using Microsoft.Win32;
using System;

namespace MyKindom.UnitSystem
{
	public partial class UnitManager : Node
	{
		public static UnitManager Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new UnitManager();
				}
				return instance;
			}
		}

		private static UnitManager instance;

		// 根据ID寻找目标单位
		public bool GetTargetUnitById(string unitId, ref UnitBase targetUnit)
		{
			GD.Print("Enter get target unit by id");
			foreach (UnitBase unit in GetChildren())
			{
				if (unit.m_unitId == unitId)
				{
					GD.Print("Find unit success");
					targetUnit = unit;
					return true;
				}
			}
			return false;
		}

		public void SetUnitPositionById(string unitId, Vector2 position)
		{
			GD.Print("Enter set unit position");
			UnitBase unit = new UnitBase();
			bool ret = GetTargetUnitById(unitId, ref unit);
			if (ret)
			{
				unit.Position = position;
			}
		}

		public void AddTargetUnitToManager(UnitBase unit)
		{
			GD.Print("Enter add target unit to manager");
			AddChild(unit);
		}
	}
}