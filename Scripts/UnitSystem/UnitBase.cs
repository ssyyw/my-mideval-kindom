using Godot;
using System;

namespace MyKindom.UnitSystem
{
	public partial class UnitBase : CharacterBody2D
	{
		[Export]
		public string m_unitName { get; set; } = "";

		[Export]
		public string m_unitType { get; set; } = "";

		[Export]
		public string m_unitId { get; set; } = "";

		[Export]
		public int m_unitBaseHealthLimit { get; set; } = 100;

		[Export]
		public int m_unitBaseSatietyLimit { get; set; } = 100;

		[Export]
		public int m_unitBaseEnergyLimit { get; set; } = 100;

		[Export]
		public int m_unitBaseSpiritLimit { get; set; } = 100;

		[Export]
		public int m_speed { get; set; } = 400;

		[Export]
		public TileMap m_tileMap { get; set; }

        [Export]
		public string m_currentTask { get; set; } = "Lumber";


		public Vector2 m_targetPosition { get; set; } = Vector2.Zero;
		private Vector2 m_moveDirection;

		private StateMachine.StateMachine m_stateMachine;
		private NavigationAgent2D m_navAgent;
		

		private int m_woodCount = 0;
		private bool m_navigationSwitch = false;
		private Vector2 m_nexPathyPosition;


        public override void _Ready()
		{
			GD.Print(this.m_unitName);
			m_stateMachine = this.GetNode<StateMachine.StateMachine>("StateMachine");
			m_navAgent = this.GetNode<NavigationAgent2D>("UnitNavigationAgent");

			m_navAgent.SetNavigationMap(m_tileMap.GetLayerNavigationMap(0));
		}

		public override void _Process(double delta)
		{
            if (Input.IsActionJustPressed("KeyBoardPressC"))
            {
                GD.Print("Press c");
                m_navigationSwitch = true;
            }

            if (Input.IsActionJustPressed("KeyBoardPressE"))
            {
                m_navigationSwitch = false;
            }
        }

        public override void _PhysicsProcess(double delta)
        {
            MoveWithNavigation(delta);
        }

		public void MoveWithNavigation(double delta)
		{
            if (m_navigationSwitch && Input.IsActionJustPressed("MouseRightClick"))
            {
                GD.Print("Press Right Click");
                m_navAgent.TargetPosition = GetGlobalMousePosition();
            }

            if (!m_navAgent.IsNavigationFinished())
			{
				GD.Print("Enter to move with navigation");

                m_nexPathyPosition = m_navAgent.GetNextPathPosition();
				m_moveDirection = this.Position.DirectionTo(m_nexPathyPosition);

				GD.Print(m_nexPathyPosition);
				GD.Print(m_moveDirection);

				this.Velocity = m_moveDirection.Normalized() * m_speed;
                GD.Print(Velocity);
                this.MoveAndCollide(this.Velocity * (float)delta);
			}
			else
			{
				this.Velocity = Vector2.Zero;
			}
        }

        public Vector2 GetTargetPosition()
		{
			return GetGlobalMousePosition();
		}

		public void SetUnitState(string newState)
		{
			m_stateMachine.SetUnitState(newState);
		}

		public void GoBackToLastState()
		{
			m_stateMachine.SetUnitState(m_stateMachine.GetLastState());
		}

		public bool AddUnitResource(int num)
		{
			m_woodCount++;
			GD.Print("当前单位总共有木材： ", this.m_woodCount);
			return true;
		}
	}
}

