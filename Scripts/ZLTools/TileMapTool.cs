﻿using System;
using System.Collections.Generic;
using MyKindom.GameError;
using Godot;
using Newtonsoft.Json;

namespace ZLTools
{
    public class TileMapTool
    {
        public static TileMapTool Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TileMapTool();
                }
                return instance;
            }
        }

        private static TileMapTool instance;

        private TileMapTool()
        {

        }

        public int GetTilePosition(string tileName)
        {
            return (int)GameErrorCode.OPERATE_SUCCESS;
        }
    }
}
