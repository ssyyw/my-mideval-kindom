﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Godot;
using MyKindom.GameError;

namespace ZLTools
{
    public class GuiTool
    {
        public static GuiTool Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GuiTool();
                }
                return instance;
            }
        }

        private static GuiTool instance;

        private GuiTool()
        {

        }

        public int GetScreenSizeAndResizeObjectToTarget()
        {
            // The size of view port need to get a canvalitem
            //Vector2 windowSize = GetViewportRect().Size;

            return (int)GameErrorCode.OPERATE_SUCCESS;
        }
    }
}
