using System.Collections.Generic;
using Godot;


namespace TaskSystem
{
    public class TaskLumber : TaskBase
    {
        public new Vector2 taskPosition = new Vector2();
        public new string taskType = "Lumber";
        public new Stack<TaskBase> taskAchieveWay;

    }
}