using System.Collections.Generic;
using Godot;

namespace TaskSystem
{
	public class TaskBase
	{
		public Vector2 taskPosition = new Vector2();
		public string taskType = "Lumber";
		public Stack<TaskBase> taskAchieveWay;
	}
}