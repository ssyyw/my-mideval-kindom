using BuildingSystem;
using Godot;
using MyKindom.GameError;
using System;
using ZLTools;

namespace MyKindom.BuildingSystem
{
    public partial class BuildingSystem : Node2D
    {
        [Export]
        public TileMap constructionLayer { get; private set; }

        [Export]
        public CanvasLayer previewLayer { get; private set; }

        [Export]
        public BuildingPlacer buildingPlacer { get; private set; }

        private string currentChooseBuildingName;
        private Building currentChooseBuilding;
        private static int buildingIndex;

        public void _ready()
        {
            Test_Ready();
        }

        public void Test_Ready()
        {
            JsonDataReader.Instance.ClassTest();
            currentChooseBuildingName = "Well";
            GetBuildingFromBuildingName(currentChooseBuildingName, ref currentChooseBuilding);
            SelectBuilding(currentChooseBuilding);
        }

        public override void _Process(double delta)
        {
            if (Input.IsActionJustPressed("KeyBoardPressF"))
            {
                currentChooseBuildingName = Test_GetNewBuilding();

                int ret = GetBuildingFromBuildingName(currentChooseBuildingName, ref currentChooseBuilding);
                if (ret != (int)GameErrorCode.OPERATE_SUCCESS)
                {
                    GD.Print("get name");
                    GD.PushError("Error: Failed to get building name.");
                }

                ret = SelectBuilding(currentChooseBuilding);
                if (ret != (int)GameErrorCode.OPERATE_SUCCESS)
                {
                    GD.PushError("Error: Failed to select new building.");
                }
            }
        }

        public string Test_GetNewBuilding()
        {
            string buildingName = "";
            if (buildingIndex == 0)
            {
                buildingName = "Well";
                buildingIndex = 1;
                return buildingName;
            }
            if (buildingIndex == 1)
            {
                buildingName = "Target";
                buildingIndex = 0;
                return buildingName;
            }
            return buildingName;
        }

        public int GetBuildingFromBuildingName(string buildingName, ref Building buildingFromName)
        {
            BuildingData buildingData = new BuildingData();
            int ret = JsonDataReader.Instance.GetBuildingData(buildingName, ref buildingData);
            if (ret != (int)GameErrorCode.OPERATE_SUCCESS)
            {
                GD.PrintErr("Error: Failed to get building from building name.");
                return (int)GameErrorCode.OPERATE_FAILED;
            }
            buildingFromName = new Building(buildingData);
            return (int)GameErrorCode.OPERATE_SUCCESS;
        }

        public int SelectBuilding(Building selectedBuilding)
        {
            int ret = buildingPlacer.GetBuilding(selectedBuilding);
            if (ret == (int)GameErrorCode.OPERATE_SUCCESS)
            {
                return (int)GameErrorCode.OPERATE_SUCCESS;
            }
            GD.PrintErr("Error: Failed to select building.");
            return (int)GameErrorCode.OPERATE_FAILED;
        }
    }
}