using Godot;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MyKindom.BuildingSystem
{
    public struct BuildingData
    {
        public string BuildingName;
        public string BuildingType;
        public int TileSetIndex;
        public Vector2I TileMapPosition;
        public string PhysicLayer;
        public string NavigationType;
        public List<string> BuildingComponents;

        public BuildingData()
        {
            BuildingName = "None";
            BuildingType = "None";
            TileSetIndex = -1;
            TileMapPosition = new Vector2I();
            PhysicLayer = "None";
            NavigationType = "None";
            BuildingComponents= new List<string>();
        }

        public override string ToString()
        {
            GD.Print(BuildingName);
            GD.Print(BuildingType);
            GD.Print(TileSetIndex);
            GD.Print(TileMapPosition);
            GD.Print(PhysicLayer);
            GD.Print(NavigationType);
            return BuildingName;
        }
    }

    public partial class Building : StaticBody2D
    {
        private BuildingData buildingData;

        public Building()
        {
            buildingData.BuildingName = "None";
        }

        public Building(BuildingData getBuildingData)
        {
            buildingData = getBuildingData;
            InitBuildingComponents();
        }

        public void InitBuildingComponents()
        {
            
        }

        public string GetBuildingName()
        {
            return buildingData.BuildingName;
        }

        public string GetBuildingType()
        {
            return buildingData.BuildingName;
        }

        public int GetBuildingTileSetIndex()
        {
            return buildingData.TileSetIndex;
        }

        public Vector2I GetTileMapPosition()
        {
            return buildingData.TileMapPosition;
        }

        public string GetPhysicLayer()
        {
            return buildingData.PhysicLayer;
        }

        public string GetNavigationType()
        {
            return buildingData.NavigationType;
        }
    }
}
