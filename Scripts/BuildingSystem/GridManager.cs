using Godot;
using System;

namespace BuildingSystem
{
	public partial class GridManager : Node
	{
		public enum StateType
		{
			NoRun = 0,
			Pause = 1,
			Running = 2,
			Building = 3,
		}

		public static GridManager Instance { get; private set; }
		public StateType stateType => _state;
		private StateType _state;

		[Export] public Node2D OriginPoint;
		[Export] public int GridExpandLength = 10;
		[Export] public Vector2 TileSize = new Vector2(1, 1);
		[Export] public PackedScene CubePrefab;
		[Export] public Node2D WorkRoot;


		public override void _Ready()
		{
			Instance = this;
		}

		// Called every frame. 'delta' is the elapsed time since the previous frame.
		public override void _Process(double delta)
		{
		}
	}
}