using System;
using System.Collections.Generic;

namespace GoapSystem
{
    public class GoapBaseData
    {
        public Dictionary<string, object> m_Data;

        public GoapBaseData()
        {
            m_Data = new Dictionary<string, object>();
        }

        /**
        * @description:检查对象是否满足愿望要求
        * @return {*}
        */
        public bool Contain(GoapBaseData targetData)
        {
            foreach (var targetKvp in targetData.m_Data)
            {
                if (m_Data.TryGetValue(targetKvp.Key, out var globalKvpValue))
                {
                    bool isContain = false;
                    isContain |= targetKvp.Value is bool && globalKvpValue.Equals(targetKvp.Value);
                    isContain |= targetKvp.Value is float && (float)globalKvpValue >= (float)targetKvp.Value;
                    isContain |= targetKvp.Value is int && (int)globalKvpValue >= (int)targetKvp.Value;
                    isContain |= targetKvp.Value is string && globalKvpValue.Equals(targetKvp.Value);
                    if (isContain)
                    {
                        continue;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        // 将自身对象完整拷贝出来
        public GoapBaseData DeepCopy()
        {
            var newData = new GoapBaseData
            {
                m_Data = new Dictionary<string, object>(this.m_Data)
            };
            return newData;
        }

        public void ModifyFromOther(GoapBaseData target)
        {
            var localData = (target as GoapBaseData);
            foreach (var localKvp in localData.m_Data)
            {
                m_Data[localKvp.Key] = localKvp.Value;
            }
        }

        public void Set(Enum enumKey, object value)
        {
            Set(enumKey.ToString(), value);
        }

        public void Set(string strKey, object value)
        {
            m_Data[strKey] = value;
        }

        public bool GetBool(Enum enumKey)
        {
            if (TryGet(enumKey, out var value))
            {
                return (bool)value;
            }

            return false;
        }

        public bool TryGet(Enum enumKey, out object value)
        {
            if (m_Data.TryGetValue(enumKey.ToString(), out value))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}