using System;

namespace GoapSystem
{

    public class GoalData
    {
        public GoalData()
        {
            m_Data = new GoapBaseData();
        }

        public GoapBaseData m_Data { get; private set; }

        // 目标优先级
        public int GoalPriority;

        // 通过传参设置
        public void SetGoal(Enum enumKey, object value)
        {
            m_Data.Set(enumKey, value);
        }

        // 通过复制设置
        public void SetGoal(GoapBaseData data)
        {
            m_Data = data;
        }
    }

}