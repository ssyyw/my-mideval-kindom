﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Godot;

namespace MyKindom.GameError
{
    public enum GameErrorCode
    {
        OPERATE_SUCCESS = 0,
        OPERATE_FAILED = 1,
    }
}
