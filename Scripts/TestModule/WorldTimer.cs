using Godot;
using System;
using System.Runtime.CompilerServices;
using MyKindom.UnitSystem;

namespace TestModule
{
	public partial class WorldTimer : Timer
	{
		[Export]
		public int internalTime = 5;

		[Export]
		public UnitSpawner unitSpawner;

		public override void _Ready()
		{
			this.WaitTime = internalTime;
			this.OneShot = false; // 设置为false，使计时器循环
			this.Start(5);

			unitSpawner.SpawnUnit();
		}
	}
}