using Godot;
using System;
using MyKindom.UnitSystem;

namespace TestModule
{
	public partial class PlantBase : StaticBody2D
	{
		[Export]
		public Area2D area;
		public override void _Ready()
		{

		}

		public override void _Process(double delta)
		{

		}

		private void _on_area_2d_body_entered(UnitBase unit)
		{
			GD.Print("");
		}
	}
}