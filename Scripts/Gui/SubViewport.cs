using Godot;
using System;

namespace MyKindom.Gui
{
    public partial class SubViewport : Godot.SubViewport
    {
        public Camera2D camara;

        public override void _Ready()
        {
            camara = GetNode<Camera2D>("MapCamara");
            camara.Position = new Vector2(640, 360);
        }

        public override void _PhysicsProcess(double delta)
        {
        }
    }
}