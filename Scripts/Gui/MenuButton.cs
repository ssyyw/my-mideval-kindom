using Godot;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using ZLTools;

public partial class MenuButton : Button
{
	private ScrollContainer scrollContainer;

	[Export]
	public bool isBottomBar = false;

	public override void _Ready()
	{
        AddMenuScrollList();
    }

	private void AddMenuScrollList()
	{
        scrollContainer = new ScrollContainer();
        if (isBottomBar)
        {
            scrollContainer.Position = new Vector2(this.Position.X, this.Position.Y - this.Size.Y);
        }
        else
        {
            scrollContainer.Position = new Vector2(this.Position.X, this.Position.Y + this.Size.Y);
        }

        this.AddChild(scrollContainer); 
    }

    private void AddMenuItem()
    {
        string dataName = this.Text + "Data";
        List<JObject> itemInfoList = new List<JObject>();
        JsonDataReader.Instance.GetAllMainObjectInJObject(dataName, itemInfoList);

        foreach (JProperty property in itemInfoList.Properties())
        {
            Button button = new Button();
            button.Size = new Vector2(100, 50);
            button.Text = property.Name;
            scrollContainer.AddChild(button);
        }
    }

	public override void _Process(double delta)
	{
	}
}
