# 主空间相机控制

extends Node2D

@onready var camera : Camera2D = get_node("MainGameCamera")
@onready var mouse_pos : Vector2 = Vector2()
@onready var window_size : Vector2 = Vector2()
@onready var target_zoom : Vector2 = Vector2()

@export var camera_move_speed : int = 400
@export var camera_move_change_rate : float = 1.1

@export var camera_zoom_min_level : Vector2 = Vector2(0.4, 0.4)
@export var camera_zoom_max_level : Vector2 = Vector2(3, 3)
@export var camera_zoom_change_rate : float = 1.1
@export var camera_zoom_lerp_value : float = 0.1

const DRAG_DISTANCE_LIMIT : int = 20

func _ready():
	#start_show()
	target_zoom = camera.zoom

func start_show():
	print(camera.position)
	print(camera_move_speed)
	print(camera_zoom_change_rate)
	
func _process(delta):
	get_sub_info()
	move_camera(delta)
	zoom_camera()

func get_sub_info():
	mouse_pos = get_viewport().get_mouse_position()
	window_size = get_viewport().get_visible_rect().size

func move_camera(delta):
	move_camera_by_keboard(delta)
	#move_camera_by_middle_drag(delta)
	camera.position = camera.position.round()
	
func move_camera_by_keboard(delta):
	var keyboard_direction = Input.get_vector("KeyBoardPressA", "KeyBoardPressD", "KeyBoardPressW", "KeyBoardPressS")
	#change_camera_move_speed_by_zoom_level()
	camera.position += keyboard_direction * camera_move_speed * delta
	
func move_camera_by_middle_drag(delta):
	print("move the camera by midle drag")

func zoom_camera():
	if Input.is_action_just_pressed("MouseUpWheel"):
		target_zoom *= camera_zoom_change_rate
		if target_zoom > camera_zoom_max_level:
			target_zoom = camera_zoom_max_level
	if Input.is_action_just_pressed("MouseDownWheel"):
		target_zoom /= camera_zoom_change_rate
		if target_zoom < camera_zoom_max_level:
			target_zoom = camera_zoom_min_level
	
	camera.zoom = camera.zoom.lerp(target_zoom, camera_zoom_lerp_value)

func change_camera_move_speed_by_zoom_level():
	if camera.zoom.x >= 0.4 and camera.zoom.x <= 0.6:
		camera_move_speed = 800
	if camera.zoom.x >= 0.6 and camera.zoom.x <= 0.8:
		camera_move_speed = 550
	if camera.zoom.x >= 0.8 and camera.zoom.x <= 1:
		camera_move_speed = 450
	if camera.zoom.x >= 1 and camera.zoom.x <= 2:
		camera_move_speed = 350
	if camera.zoom.x >= 2 and camera.zoom.x <= 3:
		camera_move_speed = 250
