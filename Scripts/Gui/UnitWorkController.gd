extends Object

class_name UnitWorkController

var unit_workable_task_list : Dictionary = {
	
}

var maintain_work_type : Array[String] = [
	"eat", "drink", "sleep", "seekheal", "flee", "communication", "play", "relex"
]

var dispatch_work_type : Array[String] = [
	"lumber", "mine", "heal", "battle", "trans", "smelt" , "forge" , "build" 
]

var indicate_work_type : Array[String] = [
	"attack", "defend", "patrol", "forcedo"
]

var unit_work_status : Dictionary = {
	unit_work_status = "working",
	unit_work_list = [],
}
