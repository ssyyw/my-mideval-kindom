using Godot;
using System;
using System.Collections.Generic;

namespace MyKindom.Gui
{
    public partial class ToolBars : GridContainer
    {
        // Called when the node enters the scene tree for the first time.
        [Export]
        public int gridColumns;

        [Export]
        public int gridRows;

        [Export]
        public Vector2 buttonMinSize;

        private List<Button> buttonList;

        public override void _Ready()
        {
            this.Columns = gridColumns;
            InitToolsButtons();
        }

        public override void _PhysicsProcess(double delta)
        {
        }

        private void InitToolsButtons()
        {
            for (int i = 0; i < gridColumns * gridRows; i++)
            {
                Button button = new Button();
                button.CustomMinimumSize = buttonMinSize;
                button.Text = "xx" + i.ToString();
                this.AddChild(button);
                //button.Connect("pressed", this, nameof(OnButtonPressed), new Godot.Collections.Array { i });
            }
        }
    }
}