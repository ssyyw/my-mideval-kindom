using Godot;
using MyKindom.UnitSystem;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata;

namespace MyKindom.StateMachine
{
	public partial class StateMachine : Node
	{
		[Export]
		public UnitBase m_currentUnit;

		private string m_currentState;
		private string m_newState;

		private string m_lastState;

		public override void _Ready()
		{
			m_currentState = "Idle";
			m_lastState = "Idle";
			m_newState = m_currentState;
		}

		public override void _Process(double delta)
		{
			TransferState();
			UnitStateDictionary.Instance.m_stateDictionary[m_currentState].UpdataState(m_currentUnit);
		}

		private void TransferState()
		{
			if (m_newState != m_currentState)
			{
				UnitStateDictionary.Instance.m_stateDictionary[m_currentState].LeaveState(m_currentUnit);
				m_currentState = m_newState;
				UnitStateDictionary.Instance.m_stateDictionary[m_currentState].EnterState(m_currentUnit);
			}
		}

		public void SetUnitState(string newState)
		{
			if (newState != m_currentState)
			{
				m_newState = newState;
				m_lastState = m_currentState;
			}
		}

		public string GetLastState()
		{
			return m_lastState;
		}
	}
}