using Godot;
using System;
using MyKindom.UnitSystem;

namespace MyKindom.StateMachine
{
    public class IdleState : UnitState
    {
        public new string m_stateName = "Idle";

        public override void EnterState(UnitBase unit)
        {
            //GD.Print("Enter IdleState");
        }

        public override void LeaveState(UnitBase unit)
        {
            //GD.Print("Leave IdleState");
        }

        public override void UpdataState(UnitBase unit)
        {
            //GD.Print("Update State");
        }
    }
}
