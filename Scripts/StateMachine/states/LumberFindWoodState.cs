using Godot;
using System;
using System.Collections.Generic;
using MyKindom.UnitSystem;

namespace MyKindom.StateMachine
{
	public class LumberFindWoodState : UnitState
	{
		public new string m_stateName = "LumberFindWood";

		public override void EnterState(UnitBase unit)
		{
			GD.Print("Enter LumberFindWoodState");
			unit.m_targetPosition = new Vector2(1156, 531);
		}

		public override void LeaveState(UnitBase unit)
		{
			GD.Print("Leave LumberFindWoodState");
		}

		public override void UpdataState(UnitBase unit)
		{
			GD.Print("Find wood to get lumber work");
			if (CheckIsFindSuccess(unit))
			{
				unit.SetUnitState("LumberGetWood");
			}
		}

		private bool CheckIsFindSuccess(UnitBase unit)
		{
			return true;
		}
	}
}

