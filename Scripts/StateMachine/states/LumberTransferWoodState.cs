using Godot;
using System;
using System.Collections.Generic;
using MyKindom.UnitSystem;

namespace MyKindom.StateMachine
{
	public class LumberTransferWoodState : UnitState
	{
		public new string m_stateName = "LumberTransferWood";

		public override void EnterState(UnitBase unit)
		{
			GD.Print("Enter LumberTransferWoodState");
			unit.m_targetPosition = new Vector2(0, 0);
		}

		public override void LeaveState(UnitBase unit)
		{
			GD.Print("Leave LumberTransferWoodState");
		}

		public override void UpdataState(UnitBase unit)
		{
			GD.Print("Find wood to get lumber work");
			unit.SetUnitState("MoveToPosition");

			bool ret = CheckCanGiveResource(unit);
			if (ret)
			{
				unit.SetUnitState("LumberFindWood");
			}
			else
			{
				unit.m_targetPosition = new Vector2(0, 0);
				unit.SetUnitState("MoveToPosition");
			}
		}

		private bool CheckCanGiveResource(UnitBase unit)
		{
			Vector2 startPosition = unit.Position;
			Vector2 endPosition = unit.m_targetPosition;
			float distance = startPosition.DistanceTo(endPosition);
			GD.Print("距离为 ", distance);

			if (distance <= 20)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}