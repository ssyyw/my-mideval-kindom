using Godot;
using System;
using System.Collections.Generic;
using MyKindom.UnitSystem;
using MyKindom.StateMachine;

// 状态池
namespace MyKindom.StateMachine
{
    public class UnitStateDictionary
    {
        public static UnitStateDictionary Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UnitStateDictionary();
                }
                return instance;
            }
        }

        public Dictionary<string, UnitState> m_stateDictionary = new Dictionary<string, UnitState>();

        private static UnitStateDictionary instance;

        private UnitStateDictionary()
        {
            m_stateDictionary.Add("Idle", new IdleState());
            m_stateDictionary.Add("MoveToPosition", new MoveToPositionState());
            m_stateDictionary.Add("LumberFindWood", new LumberFindWoodState());
            m_stateDictionary.Add("LumberGetWood", new LumberGetWoodState());
            m_stateDictionary.Add("LumberTransferWood", new LumberTransferWoodState());
        }
    }
}
