using Godot;
using System;
using System.Collections.Generic;
using MyKindom.UnitSystem;

namespace MyKindom.StateMachine
{
	public class UnitState
	{
		public string m_stateName { get; set; }

		// 进入状态
		public virtual void EnterState(UnitBase unit)
		{
			GD.Print("Enter State");
		}

		// 离开状态
		public virtual void LeaveState(UnitBase unit)
		{
			GD.Print("Leave State");
		}

		// 处于状态
		public virtual void UpdataState(UnitBase unit)
		{
			GD.Print("Update State");
		}
	}
}
