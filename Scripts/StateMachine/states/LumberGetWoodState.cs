using Godot;
using System;
using System.Collections.Generic;
using MyKindom.UnitSystem;

namespace MyKindom.StateMachine
{
    public class LumberGetWoodState : UnitState
    {
        public new string m_stateName = "LumberGetWood";

        public override void EnterState(UnitBase unit)
        {
            GD.Print("Enter LumberGetWoodState");
        }

        public override void LeaveState(UnitBase unit)
        {
            GD.Print("Leave LumberGetWoodState");
        }

        public override void UpdataState(UnitBase unit)
        {
            bool ret = CheckCanGetWood(unit);
            if (ret)
            {
                GetWood(unit);
                unit.SetUnitState("LumberTransferWood");
            }
            else
            {
                unit.m_targetPosition = new Vector2(1156, 531);
                unit.SetUnitState("MoveToPosition");
            }
        }

        private bool CheckCanGetWood(UnitBase unit)
        {
            Vector2 startPosition = unit.Position;
            Vector2 endPosition = unit.m_targetPosition;
            float distance = startPosition.DistanceTo(endPosition);
            GD.Print("距离为 ", distance);

            if (distance < 20)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void GetWood(UnitBase unit)
        {
            unit.AddUnitResource(5);
            GD.Print("Get wood success");
        }
    }
}
