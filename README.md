<!--
 * @Author: ssyyw 2294779310@qq.com
 * @Date: 2024-02-27 00:37:35
 * @LastEditors: ssyyw 2294779310@qq.com
 * @LastEditTime: 2024-02-27 00:46:52
 * @FilePath: \my-mideval-kindom\README.md
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
-->
# MyMidevalKindom

#### 介绍
基于GODOT游戏引擎开发的中世纪模拟经营游戏

#### 软件架构
> 相关图片使用DRAW.IO、PlantUML、Excalidraw绘制

![alt text](documents/images/game_layer.png)
![alt text](documents/images/word_scene_tree.png)
![alt text](documents/images/project_goal_pool.png)

#### 安装教程
1.  从源码仓下载项目
2.  从资源库下载资源，将game_resources文件夹放在项目根目录中（即存在project.godot文件的目录）
3.  下载、安装、配置.NET框架（游戏有使用C#开发的部分）

> 资源链接：
> https://pan.baidu.com/s/1Lxz9qTNoS6Kp2NRnpF7Zvg?pwd=0000 提取码: 0000 (已失效)
> 
> 暂不更新，原型开发完后创建页面下载

#### 使用说明

1.  在安装完成后打开GODOT（必须是4.X以上的mono版本）
2.  编译运行游戏

#### 参与贡献

1.  获得仓库主许可，新建develop_XX分支（XX为自定义的分支名）
2.  编译代码，本地验证通过不影响原有功能
3.  新建合并请求
4.  仓库主合并


#### 特性
##### 第一阶段：
1. 完成测试场景规范化搭建
2. 完成建筑系统基础搭建
3. 完成单位组件设计与基础实现
   1. 状态机
   2. 装备
4. 完成各渲染层、物理层统一，地形、建筑参数录入。
5. 搭建后台AI框架，为第二阶段以C#开发AI原型做准备
6. 项目基础GUI绘制与节点划分。